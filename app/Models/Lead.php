<?php

namespace App\Models;

use Cartalyst\Sentinel\Users\EloquentUser;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;

#class Lead extends EloquentUser implements AuthenticatableContract, CanResetPasswordContract {
#    use Authenticatable, CanResetPassword;
class Lead extends EloquentUser {

    protected $table="leads";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agent_id','sphere_id','name', 'customer_id', 'comment', 'date', 'bad'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    #protected $hidden = [
    #    'password', 'remember_token',
    #];

    public function sphere(){
        return $this->hasOne('App\Models\Sphere', 'id', 'sphere_id');
    }

    public function info(){
        return $this->hasMany('App\Models\LeadInfoEAV','lead_id','id');
    }

    public function phone(){
        return $this->hasOne('App\Models\Customer','id','customer_id');
    }

    public function obtainedBy($agent_id=NULL){
        $relation=$this->belongsToMany('App\Models\Agent','open_leads','lead_id','agent_id');
        return ($agent_id)? $relation->where('agent_id','=',$agent_id) : $relation;
    }

    public function getByUid($uid){
        $leads = DB::table('leads')
            ->join('customers', 'customers.id', '=', 'customer_id')
            ->join('open_leads', 'open_leads.lead_id', '=', 'leads.id')
            ->select(['date', 'name', 'email', 'customers.phone', 'sphere_id', 'leads.id'])
            ->where('open_leads.agent_id', '=', $uid)
            ->get();

        return $leads;

    }

    public function getById($id){
        $currentLeads = DB::table('leads')
            ->join('customers', 'customers.id', '=', 'customer_id')
            ->join('open_leads', 'open_leads.lead_id', '=', 'leads.id')
            ->select(['date', 'name', 'email', 'customers.phone', 'sphere_id', 'leads.id'])
            ->where('leads.id', '=', $id)
            ->get();

        return $currentLeads;

    }

    public function getAttr($leads_sphere_id,$leads_id){
        $attributes = DB::table('sphere_attribute_options as sf')

            ->join('sphere_attributes','sf.sphere_attr_id','=','sphere_attributes.id')

            ->select(['sf.value','sphere_attributes.label','sphere_attributes.id as AID','sf.id as OID'])

            ->where('sf.ctype','=','agent')

            ->get();

        foreach((array)$attributes as $key=>$val){
            $column = 'fb_'.$val->AID.'_'.$val->OID;
            $bitmask =  DB::table('sphere_bitmask_'.$leads_sphere_id)
                ->select(['fb_'.$val->AID.'_'.$val->OID])
                ->where('sphere_bitmask_'.$leads_sphere_id.'.type','=','lead')
                ->where('sphere_bitmask_'.$leads_sphere_id.'.'.$column, '=',1)
                ->where('sphere_bitmask_'.$leads_sphere_id.'.user_id', '=',$leads_id)
                ->get()
            ;

            if(empty($bitmask)){
                unset($attributes[$key]);
            }

        }

        return $attributes;
    }
}
