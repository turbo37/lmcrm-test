<?php
/**
 * Created by PhpStorm.
 * User: it280371svs1
 * Date: 14.09.16
 * Time: 8:42
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Lead;
use Sentinel;
use Illuminate\Http\Request;

class AuthUserTable extends Controller
{
    private $leads;
    private $uid;
    private $leadObj;

    public function __construct()
    {
        $this->leadObj = new Lead();
    }

    public function index(){

        if(is_object(Sentinel::getUser())) {


            $this->uid = Sentinel::getUser()->id;

            $this->leads = $this->leadObj->getByUid($this->uid);

        } else {
            $this->leads = array();
        }

        return view('views.page.table')->with('leads',$this->leads);
    }

    public function getRow(Request $request){
        $leads_sphere_id = $request->get('sphere');
        $leads_id = $request->get('id');

        $currentLeads = $this->leadObj->getById($leads_id);

        $attributes = $this->leadObj->getAttr($leads_sphere_id,$leads_id);

        foreach($currentLeads as $value){
            $currentLeadsRes = (array)$value;
            foreach($attributes as $val_attr){
                $attributesArr = (array)$val_attr;
            }
            $currentLeadsRes[$attributesArr['label']] = $attributesArr['value'];

        }

        print_r(json_encode($currentLeadsRes));

    }
}