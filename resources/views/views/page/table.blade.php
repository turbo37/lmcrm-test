@extends('layouts.master')

@section('content')
        <!-- Page Content -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Table auth user</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Icon</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($leads as $key => $value)
                        <tr onclick="getRow('{!! $value->sphere_id !!}','{!! $value->id !!}')" data-toggle="modal" data-id="{!! $key !!}" data-target="#myModal" style="cursor: pointer">
                            <td>{!! $key !!}</td>
                            <td></td>
                            <td>{!! $value->date !!}</td>
                            <td>{!! $value->name !!}</td>
                            <td>{!! $value->phone !!}</td>
                            <td>{!! $value->email !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Vertical table</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tbody id="vertical-table">
                                <tr>
                                    <th>Icon</th>
                                    <td>Table cell</td>
                                </tr>
                                <tr>
                                    <th>Date</th>
                                    <td>Table cell</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>Table cell</td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td>Table cell</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>Table cell</td>
                                </tr>
                                <tr>
                                    <th>Radio</th>
                                    <td>Table cell</td>
                                </tr>
                                <tr>
                                    <th>Checkbox</th>
                                    <td>Table cell</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        {{--<div class="modal-footer">--}}
                            {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                            {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                        {{--</div>--}}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });

        function getRow(sphere_id,id){

            var postData = {};
            postData['sphere']=sphere_id;
            postData['id']=id;
            var html;
            var thArr = ['Date','Name','Phone','Email','Radio','Checkbox'];

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:  '{{ route('auth.user.table')}}',
                method: 'POST',
                data: postData,
                dataType: 'json',
                success: function(resp){
                    html = '<tr><th>Icon</th><td></td></tr>';
                    $.each(resp, function(key, value){
                        $.each(thArr, function(key_th, value_th){
//                            console.log(key_th + ":" + value_th)
                            if(value_th.toLowerCase() == key.toLowerCase()){
                                html +='<tr><th>'+value_th+'</th><td>'+value+'</td></tr>';
                            }
                        });
//                        console.log(key + ":" + value)
                    });

                    $('#vertical-table').html(html);
//                    console.log(html);
                }
            });
        }

    </script>

    </div>
@endsection